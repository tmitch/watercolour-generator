"""
  Program that will create a watercolour of the image that is given as an arguement
  - The first arguement given as an input is the path to the file.
  - The second arguement givent to the program defines the neightbourhood 
  width to be used during the generation of the final image. This will impact the look
  of the resulting image.


"""


# import the necessary packages
import numpy as np
import cv2
import sys

#Global Variables
red = 0
blue = 0
green = 0

#Function to save a file to a folder
def saveFile(s,image) :
  name =s + ".jpg"
  cv2.imwrite(name,image)

#Adds the intensity of a pixel in a gray image to the current dictionary
def add_to_dictionary(d,i,j,grey_image) :
  intensity=grey_image[i,j]
  key=intensity[0]
  if(d.has_key(intensity[0])):
    d[key]+=1
  else :
    d[key]=1
  return d

def generate_grey_image(image,grey_image,height,width):
  for i in range(0,height):
    for j in range(0,width):
      grey_image[i,j] = 0.299 * image[i,j,red] + 0.587 * image[i,j,green] + 0.114 * image[i,j,blue]
  return grey_image

"""
For each cell in grey_image c(i,j), want to calculate the histogram
and determine the most frequent pixel intensity and set the cell C(i,j)
in the resultant image to this value.
"""

def most_frequent_intensity(i,j,height,width,grey_image,neighbourhood_width):
  
  d= {}
  r= neighbourhood_width/2
  
  for m in range(-r,r):
    for n in range(-r,r):
      if ((i+m>=0) and (i+m<height) and (j+n>=0) and (j+n<width)):
        d= add_to_dictionary(d,i+m,j+n,grey_image) 

  return max(d, key=lambda i: d[i])

""" 
This function modifies the value of each pixel replacing it with
the most frequent pixel value in the neighbourhood around the pixel

The size of the neighbourhood is defined neightbourhood width
that is passed into the funciton
"""

def modify_pixels(grey_image,height,width,neighbourhood_width):
  result_image = np.zeros((height,width,1),np.uint8)
  for i in range(0,height):
    for j in range(0,width):
      # Determine the most occuring value in the list
      result_image[i,j] = most_frequent_intensity(i,j,height,width,grey_image,neighbourhood_width)
  return result_image

"""
This function creates a list of pixels
in the neightbourhood around pixel i,j
that have the same grey value intensity
as pixel i,j and returns the list
"""

def find_equal_intensity_pixels(grey_image,height,width,i,j,neighbourhood_width):
  l=[]
  l.append((i,j))
  intensity = grey_image[i,j]
  r= neighbourhood_width/2
  """
  For each pixel in the neightbourhood region around
  a pixel only add it if it has the same intensity
  """
  for m in range(-r,r):
    for n in range(-r,r):
      if ((i+m>=0) and (i+m<height) and (j+n>=0) and (j+n<width)):
        if(intensity == grey_image[i+m,j+n]):
          l.append((i+m,j+n))

  return l

"""
This function recolours the the modified pixels using the pixel
colours from the original image.

To find the new pixel colour, the surrounding neighbourhood is probed.
A list is generated from the pixels in the neighbourhood around the focused
pixel. Any pixel in the neightbourhood with the same intensity value from the
modified grey image is added to the list

Using this list, the output pixel colour is determined by averaging the rgb
intensity of each pixel stored in the list.

This is repeated for each pixel in the image. 
"""

def recolour_pixels(original_image,grey_image,height,width,neighbourhood_width):
  result_image= original_image.copy()
  for i in range(0,height):
    for j in range(0,width):
      l= find_equal_intensity_pixels(grey_image,height,width,i,j,neighbourhood_width)
      
      #Now that we have a list of cells with the same intensity
      #Calculate the average in the original image.
      total_R = 0
      total_B = 0
      total_G = 0
      #For all the pixels in the list add their 
      for t in l:
        total_R += original_image[t[0],t[1],red]
        total_B += original_image[t[0],t[1],blue]
        total_G += original_image[t[0],t[1],green]

      av_B = total_B/len(l)
      av_R = total_R/len(l)
      av_G = total_G/len(l)
      
      #set the value in the result image to these new RBG values 
      result_image[i,j,red]= av_R
      result_image[i,j,blue]= av_B
      result_image[i,j,green]= av_G
  return result_image

def main():
  
  #Set global variables
  global blue
  blue = 0
  global green
  green = 1
  global red
  red = 2

  # load the desired image
  original_image = sys.argv[1]
  if(len(sys.argv) <3):
    print "You need to give the neighbourhood width as an input."
    print "E.g. python watercolour.py test.jpg 3"
  else :  
    neighbourhood_width = int(sys.argv[2])
    image = cv2.imread(original_image)
  
    # determine the dimensions of the new image
    height, width = image.shape[:2]
  
    #Create a empty "black" image
    grey_image = np.zeros((height,width,1), np.uint8)
    grey_image = generate_grey_image(image,grey_image,height,width)

    #Fill the newly created image with a grey scale version of the original image
    generate_grey_image(image,grey_image,height,width)

    # Modify the pixels values with respect to the neightbourhood size
    modified_image = np.zeros((height,width,1), np.uint8)
    modified_image = modify_pixels(grey_image,height,width,neighbourhood_width)

    # Recolour the resulting image based off pixel colours in the original image
    coloured_image = recolour_pixels(image,modified_image,height,width,neighbourhood_width)
    s = original_image[:original_image.index('.')] + "_final"
    saveFile(s,coloured_image)

if __name__ == "__main__":
  main()