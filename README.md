# Watercolour-Generator
This program, given an input image, creates a watercolour image resembling the original image. This program works by checking the neighbourhood around each pixel in the design and finding the most common pixel colour to be used for the current pixel being checked. This results in edges in the picture becoming less defined and resulting in an output image that resembles a water colour image. The size of the neightbourhood is specified as an arguement when running the program.

How to use the program:

To run the program, 2 inputs must be given. There inputs are:
- The image to be modified
- The neightbourhood width size. 

The final execution will be similar to below:

python watercolour.py test.jpg 7
